libcpan-meta-perl (2.150010-3) UNRELEASED; urgency=medium

  [ gregor herrmann ]
  * Drop debian/tests/pkg-perl/smoke-tests, handled by pkg-perl-
    autopkgtest now.

  [ Alex Muntada ]
  * Remove inactive pkg-perl members from Uploaders.

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.3 (no changes needed)

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Laurent Baillet ]
  * fix lintian file-contains-trailing-whitespace warning

 -- gregor herrmann <gregoa@debian.org>  Wed, 02 Aug 2017 18:36:07 -0400

libcpan-meta-perl (2.150010-2) unstable; urgency=medium

  * Fix the versioned Provides of libparse-cpan-meta-perl.
    The earlier version (1.4420) was taken from an old entry in Changes but we
    effectively ship 2.150010.
    Use ${source:Upstream-Version} to calculate the provided version of
    libparse-cpan-meta-perl.
  * Lower the version in the Breaks/Replaces on libparse-cpan-meta-perl.
    Take libparse-cpan-meta-perl's latest version in the archive to avoid a
    breaks loop with perl-modules-5.24 which also provides
    libparse-cpan-meta-perl.
    Thanks to Sven Joachim for the bug report. (Closes: #866991)
  * Declare compliance with Debian Policy 4.0.0.

 -- gregor herrmann <gregoa@debian.org>  Mon, 03 Jul 2017 19:19:47 +0200

libcpan-meta-perl (2.150010-1) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Jonathan Yu from Uploaders. Thanks for your work!

  [ Nuno Carvalho ]
  * Import upstream version 2.150010
  * debian/control:
    + update standards version to 3.9.8.
    + drop libparse-cpan-meta-perl dependency.
  * debian/copyright: add Adam Kennedy copyright holder.

  [ gregor herrmann ]
  * Update versioned (build) dependencies.
  * Add Breaks/Replaces/Provides libparse-cpan-meta-perl.
    This release merges Parse::CPAN::Meta in. The module is in perl core but
    also was in a (by now removed) separate package. Let's err on the safe
    side by adding the mentioned fields as libparse-cpan-meta-perl is still in
    stable.
  * Bump debhelper compatibility level to 9.
  * autopkgtest: enable more smoke tests.

 -- gregor herrmann <gregoa@debian.org>  Sat, 03 Sep 2016 01:40:42 +0200

libcpan-meta-perl (2.150005-1) unstable; urgency=medium

  * Import upstream version 2.150005.
    Fixes "clarify CPAN::Meta::History::Meta_* licensing"
    (Closes: #783201)
  * Add information about lib/CPAN/Meta/History/Meta* to debian/copyright.

 -- gregor herrmann <gregoa@debian.org>  Fri, 26 Jun 2015 22:35:34 +0200

libcpan-meta-perl (2.150001-1) unstable; urgency=medium

  * Import upstream version 2.150001
  * Update filename in debian/libcpan-meta-perl.docs.
  * Update years of packaging copyright.

 -- gregor herrmann <gregoa@debian.org>  Wed, 06 May 2015 20:01:22 +0200

libcpan-meta-perl (2.142690-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.
  * Add debian/upstream/metadata

  * Imported upstream version 2.142690
  * Drop build-dependency on version.pm.
  * Mark package as autopkgtest-able.
  * Declare compliance with Debian Policy 3.9.6.

 -- gregor herrmann <gregoa@debian.org>  Sat, 27 Sep 2014 15:51:31 +0200

libcpan-meta-perl (2.142060-1) unstable; urgency=medium

  * New upstream release.

 -- gregor herrmann <gregoa@debian.org>  Fri, 25 Jul 2014 23:32:28 +0200

libcpan-meta-perl (2.141520-1) unstable; urgency=medium

  * New upstream release.

 -- gregor herrmann <gregoa@debian.org>  Fri, 06 Jun 2014 13:51:32 +0200

libcpan-meta-perl (2.141170-1) unstable; urgency=medium

  * New upstream release.
  * Drop avoid-new-List-Utils.patch, not needed anymore. The module works
    with any List::Util version now. Remove (build) dependency on liblist-
    moreutils-perl as well.

 -- gregor herrmann <gregoa@debian.org>  Sat, 03 May 2014 14:56:16 +0200

libcpan-meta-perl (2.140640-1) unstable; urgency=medium

  * debian/control: remove Nicholas Bamber from Uploaders on request of
    the MIA team.
  * Install CONTRIBUTING file.

  * New upstream release.
  * Refresh avoid-new-List-Utils.patch.
  * Update (build) dependencies.
  * Update years of packaging copyright.

 -- gregor herrmann <gregoa@debian.org>  Mon, 07 Apr 2014 19:07:49 +0200

libcpan-meta-perl (2.133380-2) unstable; urgency=medium

  * Team upload

  * add liblist-moreutils-perl to dependencies
    fixes test failures, as well as runtime failures, after the
    avoid-new-List-Utils.patch addittion
  * remove trailing slash from metacpan URLs

 -- Damyan Ivanov <dmn@debian.org>  Wed, 08 Jan 2014 09:27:15 +0200

libcpan-meta-perl (2.133380-1) unstable; urgency=medium

  [ gregor herrmann ]
  * New upstream release.
  * Add (build) dependency on List::Util.
  * Declare compliance with Debian Policy 3.9.5.

  [ Dominic Hargreaves ]
  * Avoid using new List::Util as it's not available in Debian
    (see #732191)

 -- Dominic Hargreaves <dom@earth.li>  Sun, 05 Jan 2014 12:22:08 +0000

libcpan-meta-perl (2.132830-1) unstable; urgency=low

  * Team upload.
  * New upstream release.
  * d/control:
    + remove required versions for some dual-lifed modules
    + shorten one line synopsis

 -- Nuno Carvalho <smash@cpan.org>  Tue, 15 Oct 2013 10:27:22 +0100

libcpan-meta-perl (2.132661-1) unstable; urgency=low

  * New upstream release.

 -- gregor herrmann <gregoa@debian.org>  Mon, 30 Sep 2013 17:02:51 +0200

libcpan-meta-perl (2.132510-1) unstable; urgency=low

  * New upstream release.
  * Switch order of alternative (build) dependencies, now that 5.18 is in
    sid.

 -- gregor herrmann <gregoa@debian.org>  Tue, 17 Sep 2013 19:01:02 +0200

libcpan-meta-perl (2.132140-1) unstable; urgency=low

  * New upstream release.

 -- gregor herrmann <gregoa@debian.org>  Fri, 09 Aug 2013 18:17:44 +0200

libcpan-meta-perl (2.131560-1) unstable; urgency=low

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * New upstream release.
  * Update years of packaging copyright.
  * Update (build) dependencies. Dropped versioned (build) depenendencies
    satisfied by perl in oldstable.
  * Set Standards-Version to 3.9.4 (no further changes).

 -- gregor herrmann <gregoa@debian.org>  Wed, 24 Jul 2013 20:10:14 +0200

libcpan-meta-perl (2.120921-1) unstable; urgency=low

  [ Alessandro Ghedini ]
  * New upstream release 2.120920
  * Email change: Alessandro Ghedini -> ghedo@debian.org
  * Update (build) depends on libparse-cpan-meta-perl, libjson-pp-perl,
    libcpan-meta-yaml-perl

  [ gregor herrmann ]
  * New upstream release 2.120921.
  * CPAN::Meta::Requirements has been split out into its own distribution.
    Add (build) dependency on libcpan-meta-requirements-perl.
  * Update debian/* section in debian/copyright.

 -- gregor herrmann <gregoa@debian.org>  Sat, 07 Apr 2012 14:16:41 +0200

libcpan-meta-perl (2.120630-1) unstable; urgency=low

  [ Alessandro Ghedini ]
  * New upstream release

  [ gregor herrmann ]
  * Bump (build) dependency on 'version'.

 -- Alessandro Ghedini <al3xbio@gmail.com>  Mon, 05 Mar 2012 18:01:48 +0100

libcpan-meta-perl (2.120530-1) unstable; urgency=low

  [ Alessandro Ghedini ]
  * New upstream release
  * Add myself to Uploaders
  * Update debian/copyright format as in Debian Policy 3.9.3
  * Bump Standards-Version to 3.9.3

  [ Salvatore Bonaccorso ]
  * Add missing Files field in copyright file paragraph.
    Files field is required in each paragraph in copyright-format 1.0.

 -- Alessandro Ghedini <al3xbio@gmail.com>  Thu, 23 Feb 2012 11:38:57 +0100

libcpan-meta-perl (2.120351-1) unstable; urgency=low

  * Team upload
  * New upstream release

 -- Alessandro Ghedini <al3xbio@gmail.com>  Sun, 05 Feb 2012 16:51:33 +0100

libcpan-meta-perl (2.113640-1) unstable; urgency=low

  * Team upload
  * New upstream release
  * Remove libversion-requirements-perl from (Build-)Depends(-Indep)
  * Add perl 5.14 as alternative (B-)D(-I) for libparse-cpan-meta-perl

 -- Alessandro Ghedini <al3xbio@gmail.com>  Fri, 30 Dec 2011 22:47:53 +0100

libcpan-meta-perl (2.112621-1) unstable; urgency=low

  * New upstream release.
  * Remove spelling patch, applied upstream.

 -- gregor herrmann <gregoa@debian.org>  Fri, 23 Sep 2011 17:28:44 +0200

libcpan-meta-perl (2.112580-1) unstable; urgency=low

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

  [ gregor herrmann ]
  * New upstream release.

 -- gregor herrmann <gregoa@debian.org>  Sat, 17 Sep 2011 16:29:07 +0200

libcpan-meta-perl (2.112150-1) unstable; urgency=low

  [ gregor herrmann ]
  * Switch order of alternative (build) dependencies after the perl 5.12
    upload.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ gregor herrmann ]
  * New upstream release.
  * Lower build dependency on Test::More.
  * Add a patch to fix a spelling mistake.

 -- gregor herrmann <gregoa@debian.org>  Sat, 06 Aug 2011 20:45:01 +0200

libcpan-meta-perl (2.110930-1) unstable; urgency=low

  [ Nicholas Bamber ]
  * New upstream release

  [ gregor herrmann ]
  * Set Standards-Version to 3.9.2 (no changes).

 -- Nicholas Bamber <nicholas@periapt.co.uk>  Thu, 07 Apr 2011 15:49:24 +0100

libcpan-meta-perl (2.110910-1) unstable; urgency=low

  * New upstream release
  * Updated copyright

 -- Nicholas Bamber <nicholas@periapt.co.uk>  Sat, 02 Apr 2011 15:32:09 +0100

libcpan-meta-perl (2.110580-1) unstable; urgency=low

  [ Ansgar Burchardt ]
  * Update my email address

  [ Nicholas Bamber ]
  * New upstream release (2.110240)

  [ Jonathan Yu ]
  * New upstream release (2.110440, 2.110550)
  * Add myself to Uploaders and Copyright

 -- Nicholas Bamber <nicholas@periapt.co.uk>  Sat, 12 Mar 2011 09:35:58 +0000

libcpan-meta-perl (2.102400-1) unstable; urgency=low

  [ Nicholas Bamber ]
  * New upstream release (2.102160).
  * Added myself to copyright and Uploaders

  [ Ansgar Burchardt ]
  * New upstream release (2.102400).
  * Remove alternate build-dep on perl (>= 5.12) for libversion-perl.
    This is correct, but causes sbuild to fail, see #586275.
  * Use debhelper compat level 8.
  * debian/copyright: Refer to /usr/share/common-licenses/GPL-1; refer to
    "Debian systems" instead of "Debian GNU/Linux systems".

 -- Ansgar Burchardt <ansgar@43-1.org>  Sun, 29 Aug 2010 20:42:34 +0900

libcpan-meta-perl (2.101670-1) unstable; urgency=low

  * New upstream release.

 -- Ansgar Burchardt <ansgar@43-1.org>  Fri, 18 Jun 2010 11:28:47 +0900

libcpan-meta-perl (2.101610-1) unstable; urgency=low

  * New upstream release.

 -- Ansgar Burchardt <ansgar@43-1.org>  Sat, 12 Jun 2010 19:12:38 +0900

libcpan-meta-perl (2.101591-1) unstable; urgency=low

  [ Ansgar Burchardt ]
  * New upstream release 2.101590.
  * Add (build-)dep on libversion-perl (>= 1:0.8200) | perl (>= 5.12).
  * Drop patch spelling.patch: applied upstream.

  [ gregor herrmann ]
  * New upstream release 2.101591.
  * Add /me to Uploaders.

 -- gregor herrmann <gregoa@debian.org>  Tue, 08 Jun 2010 18:24:53 +0200

libcpan-meta-perl (2.101461-1) unstable; urgency=low

  * Initial Release. (Closes: #583468)

 -- Ansgar Burchardt <ansgar@43-1.org>  Fri, 28 May 2010 03:10:37 +0900
